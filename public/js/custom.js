function picUpload(ele){
  var data = new FormData();
  data.append('profileImage',$(ele)[0].files[0]);

  $.ajax({
    url: '/uploadImage',
    method: 'POST',
    data: data,
    cache: false,
    processData: false,
    contentType: false,
    success: (data) => {
      $('.avatar').attr('style','background: url("uploads/profile/'+data.filename+'"); background-repeat: no-repeat; background-size: 100% 100%;');
      $('.image_path').val(data.filename);
    }
  });
}
