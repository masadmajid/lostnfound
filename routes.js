const express = require('express'),
      router = express.Router(),
      ctr = require('./controller'),
      multer  = require('multer'),

      storage = multer.diskStorage({
        destination: (req, file, cb) => {
          if(file.fieldname == 'profileImage')  cb(null, 'public/uploads/profile/');
            else cb(null, 'public/uploads/');
          },
        filename: (req, file, cb) => {
          cb(null, Date.now()+ file.originalname);
        }
      }),

      upload = multer({storage: storage});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    router.get(['/','/index'], (req, res) => {
      res.render('index');
    });

    router.get('/login', (req, res) => {
      res.render('login');
    });

    router.post('/register_user', ctr.registerUser);

    /*router.post('/register_user',function(req,res){
      client.query('INSERT INTO contact_us (name,email,subject,message) VALUES ($1,$2,$3,$4)', [req.body.name,req.body.email,req.body.subject,req.body.message]);
      res.redirect('/');
    });*/

    router.get('/controller',ctr.HomePage);

    router.post('/uploadImage', upload.single('profileImage'), ctr.ReturnImagePath);

module.exports = router;
