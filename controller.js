const model = require('./model');

module.exports = {

  registerUser : (req, res) => {
    console.log(req);
    res.redirect('login');
  },

  HomePage : function(req, res){
    res.send(model.getAll('contact_us'));
  },

  ReturnImagePath : function(req, res){
    res.send(req.file);
  }

}
