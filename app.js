const express = require('express'),
    path = require('path'),
    routes = require('./routes'),
    bodyParser = require('body-parser'),
    consolidate = require('consolidate'),
    dust = require('dustjs-helpers'),
    //bcrypt = require('bcrypt'),
    //session = require('express-session'),
    app = express();

// Assign dust engine to .dust files
app.engine('dust', consolidate.dust);

// Set Defualt ext .dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set Public Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.use('/',routes);
// Server
app.listen(3000, function(){
  console.log('Server started on Port 3000');
});
